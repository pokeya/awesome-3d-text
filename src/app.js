import * as THREE from "three";
import { OrbitControls } from "three/addons/controls/OrbitControls.js";
import { FontLoader } from "three/examples/jsm/loaders/FontLoader.js";
import { TextGeometry } from "three/examples/jsm/geometries/TextGeometry.js";
import gsap from "gsap";

// 设置画布尺寸和跟踪鼠标位置的全局变量
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};
const cursor = { x: 0, y: 0 };

// 注册鼠标移动事件监听器，用于跟踪鼠标位置
window.addEventListener("mousemove", (event) => {
  cursor.x = event.clientX / sizes.width - 0.5;
  cursor.y = -(event.clientY / sizes.height - 0.5);
});

// 监听窗口尺寸变化事件，并在窗口大小变化时更新摄像机和渲染器
window.addEventListener("resize", () => {
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

// 监听双击事件，用于进入和退出全屏
window.addEventListener("dblclick", () => {
  const fullscreenElement =
    document.fullscreenElement || document.webkitFullscreenElement;

  if (!fullscreenElement) {
    const requestFullScreen =
      canvasDom.requestFullscreen || canvasDom.webkitRequestFullscreen;
    requestFullScreen.call(canvasDom);
  } else {
    const exitFullscreen =
      document.exitFullscreen || document.webkitExitFullscreen;
    exitFullscreen.call(document);
  }
});

/**
 * 创建场景、相机、控制器、渲染器
 */

// 获取canvas元素
const canvasDom = document.querySelector("canvas.webgl");

// 创建场景
const scene = new THREE.Scene();

// 创建相机
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
);
camera.position.z = 3;
scene.add(camera);

// 创建控制器
const controls = new OrbitControls(camera, canvasDom);
controls.enableDamping = true;

// 创建渲染器
const renderer = new THREE.WebGLRenderer({ canvas: canvasDom });
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

/**
 * 创建对象（文字、几何体）
 */

// 使用TextureLoader加载背景纹理
const textureLoader = new THREE.TextureLoader();
textureLoader.load("/static/images/douyin-carnival.jpg", function (texture) {
  texture.colorSpace = THREE.SRGBColorSpace;
  scene.background = texture;
});

// 加载Matcap纹理，并创建3D文字和甜甜圈的几何体和材料
const matcapTextures = [];
for (let i = 1; i <= 8; i++) {
  const texture = textureLoader.load(`/static/textures/matcaps/${i}.png`);
  texture.colorSpace = THREE.SRGBColorSpace;
  matcapTextures.push(texture);
}

// 3D文字和甜甜圈的Meshes数组
let text;
const donuts = [];

// 使用FontLoader加载字体，然后创建文字和甜甜圈的Meshes
const fontLoader = new FontLoader();
fontLoader.load("/static/fonts/DouyinSans_Bold.json", (font) => {
  // 创建文字几何体、材料和Mesh，添加至场景
  const textGeometry = new TextGeometry("Douyin - 记录美好生活", {
    font: font,
    size: 0.5,
    depth: 0.2,
    curveSegments: 12,
    bevelEnabled: true,
    bevelThickness: 0.03,
    bevelSize: 0.02,
    bevelOffset: 0,
    bevelSegments: 5,
  });
  // 以坐标原点为中心，使文字居中
  textGeometry.center();
  const textMaterial = new THREE.MeshMatcapMaterial({
    matcap: matcapTextures[Math.floor(Math.random() * matcapTextures.length)],
  });
  text = new THREE.Mesh(textGeometry, textMaterial);
  scene.add(text);

  // 创建甜甜圈几何体、材料，并把创建的Mesh添加至场景和donuts数组
  const donutGeometry = new THREE.TorusGeometry(0.3, 0.2, 20, 45);
  for (let i = 0; i < 200; i++) {
    const donutMaterial = new THREE.MeshMatcapMaterial({
      matcap: matcapTextures[Math.floor(Math.random() * matcapTextures.length)],
    });
    const donut = new THREE.Mesh(donutGeometry, donutMaterial);

    // 定位、旋转和定标甜甜圈
    donut.position.x = (Math.random() - 0.5) * 10;
    donut.position.y = (Math.random() - 0.5) * 10;
    donut.position.z = (Math.random() - 0.5) * 10;

    // 随机旋转和缩放甜甜圈
    donut.rotation.x = Math.random() * Math.PI;
    donut.rotation.y = Math.random() * Math.PI;
    donut.scale.setScalar(Math.random());

    // 将甜甜圈Mesh添加至场景
    scene.add(donut);

    // 将甜甜圈Mesh添加至donuts数组
    donuts.push(donut);
  }
});

// 动画循环函数，用于每帧更新场景和渲染器
const animate = () => {
  // 旋转文字和每个甜甜圈
  if (text) {
    text.rotation.y += 0.005;
  }
  donuts.forEach((donut) => {
    donut.rotation.x += 0.01;
    donut.rotation.y += 0.01;
  });

  // 更新OrbitControls并渲染场景
  controls.update();
  renderer.render(scene, camera);

  // 请求下一帧动画
  requestAnimationFrame(animate);
};

// 启动动画循环
animate();

// 使用GSAP创建一个动画，使相机可以围绕scene进行旋转
gsap.to(camera.position, {
  duration: 10,
  x: 5,
  y: 5,
  z: 5,
  repeat: -1,
  yoyo: true,
  ease: "power1.inOut",
  onUpdate: () => {
    camera.lookAt(scene.position);
  },
});
